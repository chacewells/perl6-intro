#!/usr/bin/env perl6
use v6;

say 'this is say';

print 'this is print;';
say 'see? no new line!';

say 'how about getting some input with "get"?';
my $msg=get;
say "you said '$msg'";

$msg = prompt('print and get with prompt. say something: ');

say "you said '$msg'";
