multi what-is-it (Any:U) { "It's a type object!" }
multi what-is-it (Any:D) { "It's an instance!" }

say "Int: ", what-is-it Int;
say "42: ", what-is-it 42;
say "Str: ", what-is-it Str;
say "'hello mom': ", what-is-it 'hello mom';
