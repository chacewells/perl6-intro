#!/usr/bin/env perl6
use v6;

say "run doesn't open a shell";
say 'this is run:';
my $name = 'Neo';
run 'echo', "hello $name";

say 'and this is shell:';
shell 'ls';
