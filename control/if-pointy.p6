#!/usr/bin/env perl6
use v6;
my $x = 5;
if ($x) -> $a { say $a }; # i.e. alias $a as the result of the if expression

my $y = 0;
if (!$y) -> $b { say $b }; # again, the result of the conditional, which is now True
