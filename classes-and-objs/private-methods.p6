#!/usr/bin/env perl6
use v6;

class PublicPrivate {
    # '!' syntax makes a method private
    method !iamprivate {
        say 'private method running'
    }
    method iampublic {
        say 'public method about to call private';
        self!iamprivate;
        say 'done with public method'
    }
}

my $publicPrivate = PublicPrivate.new;
$publicPrivate.iampublic;
