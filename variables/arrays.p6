#!/usr/bin/env perl6
use v6;

# the standard way of creating
my @animals = ['camel', 'llama', 'owl'];
say @animals, ': array of animals';

@animals.push('owl');
say @animals, ', added owl';
@animals.pop;
say 'no more owl: ', @animals;

say 'use @ sigil for access in 6: ', @animals[0];

say 'array size can now be restricted';
my @tbl[3;2];
@tbl[0;0] = 1;
@tbl[0;1] = 'x';
@tbl[1;0] = 2;
@tbl[1;1] = 'y';
@tbl[2;0] = 3;
@tbl[2;1] = 'z';
say @tbl;
