#!/usr/bin/env perl6
use v6;

for @*ARGS -> $filename {
    spurt $filename, "#!/usr/bin/env perl6\nuse v6;\n\n";
    $filename.IO.chmod(0o755)
}
