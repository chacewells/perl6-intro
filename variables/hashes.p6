#!/usr/bin/env perl6
use v6;

my %capitals = UK => 'London', Germany => 'Berlin'; # parens no longer needed
say %capitals;

%capitals.push: (France => 'Paris');
%capitals.say;

# access now by angle brackets
say "The capital of France is: " ~ %capitals<France>;
