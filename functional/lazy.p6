#!/usr/bin/env perl6
use v6;

# default generator is +1
my $lazy = 1 ... 10;
say $lazy;

# implicit generator of +2
my $evens = 0,2 ... 10;
say $evens;

# defined generator of +3
my $threes = (0, { $_ + 3 } ... 12);
say $threes;

# when using an explicit generator, the endpoint must be one
# of the values that the generator can return

# this won't stop the generator
my $other-threes = (0, { $_ + 3 } ... 10);
say $other-threes;

# this will
my $less-than-ten = (0, { $_ + 3 } ...^ * > 10);
say $less-than-ten;
