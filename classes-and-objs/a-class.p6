#!/usr/bin/env perl6
use v6;

# this class has all private attributes
class PrivateHuman {
    has $!name;
    has $!age;
    has $!sex;
    has $!nationality;
}

# this class has automatically-generated accessors
class Human {
    has $.name;
    has $.age;
    has $.sex;
    has $.nationality;
    has $.eligible;

    # new constructor (positional)
    method new ($name,$age,$sex,$nationality) {
        self.bless(:$name,:$age,:$sex,:$nationality);
    }
    method assess-eligibility {
        # 'self.' notation calls the accessor
        # '$!' notation directly calls the variable
        if self.age < 21 { $!eligible = 'No' }
        else { $!eligible = 'Yes' }
    }
}

my $aaron = Human.new('Aaron',30,'M','American');
say $aaron; # says 'Human.new'; that's lame
say "{$aaron.name}, {$aaron.age}, {$aaron.nationality}";
say "gender is {$aaron.sex}";
$aaron.assess-eligibility;
say "Is {$aaron.name} eligible? {$aaron.eligible}";

my $madison = Human.new('Madison', 12, 'F', 'American');
say $madison;
$madison.assess-eligibility;
say "Is {$madison.name} eligible? {$madison.eligible}";
