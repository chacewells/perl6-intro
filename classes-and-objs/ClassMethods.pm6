class ClassMethods is export {
    my Bool $.yes;
    my $private;
    method make-it-no {
        $.yes = False;
    }
    method make-it-yes {
        $.yes = True;
    }
    method show-private { $private }
    method set-private($a) { $private = $a }
}
