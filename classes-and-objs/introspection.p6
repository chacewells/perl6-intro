#!/usr/bin/env perl6
use v6;

class Human {
  has Str $.name;
  has Int $.age;
  method introduce-yourself {
    say 'Hi i am a human being, my name is ' ~ self.name;
  }
}

class Employee is Human {
  has Str $.company;
  has Int $.salary;
  method introduce-yourself {
    say 'Hi i am a employee, my name is ' ~ self.name ~ ' and I work at: ' ~ self.company;
  }
}

my $john = Human.new(name =>'John',age => 23,);
my $jane = Employee.new(name =>'Jane',age => 25,company => 'Acme',salary => 4000);

say "john WHAT: ", $john.WHAT;
say "jane WHAT: ", $jane.WHAT;
say "john attributes: ", $john.^attributes;
say "jane attributes: ", $jane.^attributes;
say "john methods: ", $john.^methods;
say "jane methods: ", $jane.^methods;
say "jane parents: ", $jane.^parents;
if $jane ~~ Human {say 'Jane is a Human'};

say "jane's attributes:";
say "\tname: ", .name, '; type: ', .type for $jane.^attributes;
