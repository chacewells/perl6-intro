#!/usr/bin/env perl6
use v6;

say 'does slurp.p6 exist? ', 'slurp.p6'.IO.e;
say 'does folder123 exist? ', 'folder123'.IO.e;
say 'does fold exist? ', 'fold'.IO.e;

say 'is slurp.p6 a directory? ', 'slurp.p6'.IO.d;
say 'is folder123 a directory? ', 'folder123'.IO.d;

say 'is slurp.p6 a file? ', 'slurp.p6'.IO.f;
say 'is folder123 a file? ', 'folder123'.IO.f;
