#!/usr/bin/env perl6
use v6;

my $var = 42;

say 'first go around. auto-break:';
given $var {
    when 0..50 { say 'Less than 50' }
    when Int { say "is an Int" }
    when 42 { say 42 }
    default { say 'huh?' }
}

say 'second go around. added "proceed" statement';
given $var {
    when 0..50 { say 'Less than 50';proceed }
    when Int { say "is an Int";proceed }
    when 42 { say 42 }
    default { say 'huh?' }
}

