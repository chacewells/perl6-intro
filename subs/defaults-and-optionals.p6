#!/usr/bin/env perl6
use v6;

# demonstrates optional arguments
sub say-hello($name?) {
    with $name { say "Hello " ~ $name }
    else { say "Hello Human" }
}
say-hello;
say-hello "Laura";

sub say-hello-default($name='Matt') {
    say "Hello $name"
}
say-hello;
say-hello "Laura";
