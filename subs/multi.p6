#!/usr/bin/env perl6
use v6;

multi greet($name) {
    say "Good morning $name"
}

multi greet($name, $title) {
    say "Good morning $title $name"
}

greet "Johnnie";
greet "Laura", "Mrs.";
