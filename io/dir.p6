#!/usr/bin/env perl6
use v6;

say 'dir function';
$_.basename.say for dir;
$_.basename.say for dir '/Users/aaronwells/Documents';

mkdir 'fake-folder';
say 'after making fake-folder:';
$_.basename.say for dir;
say 'after removing fake-folder:';
rmdir 'fake-folder';
$_.basename.say for dir;
