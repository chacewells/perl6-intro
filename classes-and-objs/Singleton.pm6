class Singleton is export {
    my Singleton $instance;
    method new {!!!}
    submethod instance {
        $instance = Singleton.bless unless $instance;
        $instance;
    }
}

sub MAIN {
    my $single = Singleton.instance;
    my $other = Singleton.instance;

    say "are other and single the same? ", $other === $single;
}
