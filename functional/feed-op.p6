#!/usr/bin/env perl6
use v6;

# feeds an array, chaining and assigning from left to right
my @array = <7 8 9 0 1 2 4 3 5 6>;
@array ==> unique()
       ==> sort()
       ==> reverse()
       ==> my @final-array;
say @array;
say @final-array;
