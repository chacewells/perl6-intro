#!/usr/bin/env perl6
use v6;

# hyper applies a method to all elements in an array
my @array = <0 1 2 3 4 5 6 7 8 9 10>;
sub is-even($var) { $var %% 2 };

say @array;
say 'is-prime:';
say @array>>.is-prime;
say 'is-even:';
say @array>>.&is-even;
