#!/usr/bin/env perl6
use v6;

say-hello "Laura";

sub say-hello (Str $name) {
    say "Hello $name!"
}
