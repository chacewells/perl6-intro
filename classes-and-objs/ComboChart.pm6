class BarChart {
    has Int @.bar-values;
    method plot { say @.bar-values }
}
class LineChart {
    has Int @.line-values;
    method plot { say @.line-values }
}
class ComboChart is BarChart is LineChart is export {}
