#!/usr/bin/env perl6
use v6;

# if i know only one exception will be thrown
my Str $name;
try {
    $name = 123;
    # try to concat Int with Str
    say "Hello " ~ $name;
    CATCH {
        default { .message.say }
    }
}

# any number of exceptions could be thrown
try {
    for @*ARGS[0] -> $fn {
        say "first line of $fn:";
        my Str $data = (slurp $fn).split("\n")[0];
        say $data;
    }
    $name = 456;
    CATCH {
        when X::AdHoc { "couldn't open the file".say }
        when X::Str::Numeric { "number coersion problem".say }
        default { .message.say }
    }
}
