#!/usr/bin/env perl6

say "here's a with statement";

my Int $var; # uninitialized, won't say hello
say "Hello" with $var;

say "Goodbye" without $var;

$var = 0;
say "Hello" with $var;

say "Goodbye" without $var;
