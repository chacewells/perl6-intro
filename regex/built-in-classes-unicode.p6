#!/usr/bin/env perl6
use v6;

# unicode classes (properties) are enclosed in <: >
if "John123" ~~ / <:N> / {
  say "Contains a number";
  say $1
} else {
  say "Doesn't contain a number"
}
if "John-Doe" ~~ / <:Lu> / {
  say "Contains an uppercase letter";
} else {
  say "Doesn't contain an upper case letter"
}
if "John-Doe" ~~ / <:Pd> / {
  say "Contains a dash";
} else {
  say "Doesn't contain a dash"
}
