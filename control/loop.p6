#!/usr/bin/env perl6
use v6;

say 'a C-style for loop. uses loop keyword in Perl 6';
loop (my $i=0; $i < 5; $i++) {
    say "The current number is $i"
}
