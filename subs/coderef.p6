#!/usr/bin/env perl6
use v6;

my $coderef = {
    print "hello ";
    say 'world!';
}

$coderef();
