class WhatsIt {
    has $.grumble is rw;
}

my $wat = WhatsIt.new(grumble => 'jungle');

$wat.grumble.say;
$wat.grumble = 'jive';

$wat.grumble.say;
