#!/usr/bin/env perl6
use v6;

# in perl6, regexes are "spacey" by default
# in other words, they don't look at whitespace
if 'enlightenment' ~~ m/ light / { # leading/trailing space ignored
    say 'enlightenment contains the word light'
}

# these are valid regex definitions
# alphanumeric (A-Za-z0-9_) are written as is
# everything else is escaped
$_ = '';
/light/;
m/light/;
rx/light/;

# backslash
if 'Temperature: 13' ~~ m/ \: / {
    say "The string provided contains a colon :";
}

# single quotes
if 'Age = 13' ~~ m/ '=' / {
    say "The string provided contains an equal character = ";
}

# double quotes
if 'name@company.com' ~~ m/ "@" / {
    say "This is a valid email address because it contains an @ character";
}


