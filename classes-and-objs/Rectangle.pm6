class Point {
    has Int $.x;
    has Int $.y;
}

class Rectangle is export {
    has Point $.lower;
    has Point $.upper;

    method area() returns Int {
        ($!upper.x - $!lower.y) * ($!upper.y - $!lower.y)
    }
}

sub MAIN {
    my $r = Rectangle.new(lower => Point.new(x => 0, y => 0), upper => Point.new(x => 10, y => 10));
    say "area of r: $r.area()";
}
