package Things is export {
    class People is export {
        has $.nation;
    }
    class Duple is export {
        has $.two-step;
        submethod BUILD(:$!two-step) {
            welcome;
        }
    }
    our sub welcome { say "this is the class of Things!" }
}
