#!/usr/bin/env perl6
use v6;

# the subroutine way
my @array = <1 2 3 4 5>;
sub squared($x) {
    $x ** 2
}
say map(&squared, @array);

# the anonymous function way
say @array.map(-> $x {$x ** 2});

# the anonymous function scalar way
my $squared = -> $x {$x**2};
say $squared(9);
say map($squared, @array);
