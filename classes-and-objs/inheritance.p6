#!/usr/bin/env perl6
use v6;

class Human {
    has $.name;
    has $.age;
}

my class SuckyEmployee {
    has $.name;
    has $.age;
    has $.company;
    has $.salary;
}
# that sucks

class BetterEmployee is Human {
    has $.company;
    has $.salary;
}

my $emp = BetterEmployee.new(
    name => 'Aaron',
    age => 30,
    company => 'Expeditors',
    salary => 62_000);

say $emp;
