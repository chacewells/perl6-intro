#!/usr/bin/env perl6
use v6;

my $var = 'Text';
say $var;
say "\$var '$var' is a ", $var.WHAT;

$var = 123;
say "\$var $var is now a ", $var.WHAT;

my Int $i; # this variable can't change types
say "\$i is a ", $i.WHAT, " even though it is uninitialized";

$i = 124;
say "\$i is now $i";
try {
    $i = 'string';
    CATCH {
        default {
            warn "That didn't work.";
        }
    }
}

say $i.flip, ' is a ', $i.flip.WHAT;
