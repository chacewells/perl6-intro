#!/usr/bin/env perl6
use v6;

my @numbers = [7,2,4,9,11,3];

say 'mutating @numbers with push (push is a mutator)';
@numbers.push(99);
say @numbers;      #1

say 'non-mutating sort function';
say @numbers.sort; #2
say @numbers;      #3

say 'forcing mutation with .= syntax';
@numbers.=sort;
say @numbers;      #4
