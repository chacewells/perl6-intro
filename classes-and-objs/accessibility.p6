#!/usr/bin/env perl6
use v6;

class Human {
    has $.name;
    has $.age is rw; # makes an attribute mutable
}

my $john = Human.new(name => 'John', age => 21);
say $john;

$john.age = 23;
say $john;
