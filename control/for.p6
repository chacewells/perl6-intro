#!/usr/bin/env perl6

# for has a new syntax

my @array = 1,2,3;

for @array -> $array-item {
    say $array-item*100
}

say $_*50 for @array;
