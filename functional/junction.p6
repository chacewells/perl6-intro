#!/usr/bin/env perl6
use v6;

# junctions match a junction with == or eq
my $var = 2;
if $var == 1|2|3 {
    say "\$var is 1 or 2 or 3"
}

my $word = 'and';
if $word eq 'if'|'and'|'or' {
    say "\$word is 'if', 'and', or 'or'"
}
