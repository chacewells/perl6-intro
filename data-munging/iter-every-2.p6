#!/usr/bin/env perl6
use v6;

my @people = <Foo 123 Bar 456 Aaron 30 Jenni 25 Mom 62 Dad 62>;

for @people -> $name, $num { say "$name, $num" };
