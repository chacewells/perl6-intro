#!/usr/bin/env perl6
use v6;

my @array = <7 8 9 0 1 2 4 3 5 6 7 8 9>;
my @final-array = reverse(sort(unique(@array)));
say @final-array;

my @other-array = @array.unique.sort.reverse;
say @other-array;
