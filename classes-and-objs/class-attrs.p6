#!/usr/bin/env perl6
use v6;

class Human {
    has $.name;
    my $.counter = 0;
    method new($name) {
        Human.counter++;
        self.bless(:$name);
        say "creating Human ", $name;
    }
}
my $a = Human.new('a');
my $b = Human.new('b');

say "There are " ~ Human.counter ~ " humans";
