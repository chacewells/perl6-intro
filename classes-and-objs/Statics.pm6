# Here are the many ways to declare static variables, a.k.a. package globals

# class
class Foo {
    our $foo = 'this is a global'; # global declaration; publicly available via :: syntax
    my $bar = 'i am private'; # private with lexical declaration
    my $.baz = 'i am an attribute'; # lexical with accessor
}

say "Foo foo: $Foo::foo";
say "Foo baz: ", Foo.baz;
Foo.baz = 'something different';
say "Foo baz again: ", Foo.baz();

# package
package Bar {
    our $exposed = 'i feel so exposed'; # global declaration; use :: syntax
    my $hidden = "you can't see me"; # only available in package scope
    our sub see-hidden { $hidden } # now you see me
    our sub set-hidden { $hidden = $^a } # how you can change me
}

say "exposed: $Bar::exposed";
say "hidden: ", Bar::see-hidden;
Bar::set-hidden "that's different";
say "hidden again: ", Bar::see-hidden;
