class Human {
    has Str $.name;
    has Int $.age;
    method introduce-yourself {
        say 'Hi I am a human being, my name is ' ~ self.name;
    }
}

class Employee is Human {
    has Str $.company;
    has Int $.salary;
    method introduce-yourself {
        say 'Hi I am an employee, my name is ' ~ self.name ~ ' and I work at: ' ~  self.company;
    }
}

my $john = Human.new(name => 'John', age => 23);
my $jane = Employee.new(name => 'Jane', age => 25, company => 'Acme', salary => 4000);

say $john.WHAT;
say $jane.WHAT;
say $john.^attributes;
say $jane.^attributes;
say $john.^methods;
say $jane.^methods;
say $jane.^parents;

if $jane ~~ Human {
    say 'Jane is a Human'
}
