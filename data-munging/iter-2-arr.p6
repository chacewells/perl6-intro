#!/usr/bin/env perl6
use v6;

my @names = <Aaron Jenni Mom Chris>;
my @ages = <30 25 62 36>;

# this doesn't work as expected
for @names Z @ages -> $elem {
    say $elem.split(' ').join(', ')
}
