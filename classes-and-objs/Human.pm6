class Human is export {
    has $.name;
    has $.age is rw;
    has $.sex;
    has $.nationality;
    has $.eligible;
    my $.counter = 0; # introducing class attributes

    method new($name, $age, $sex, $nationality) {
        Human.counter++; # increment the Human count on construct
        self.bless(:$name,:$age,:$sex,:$nationality);
    }

    # adding this method to demonstrate method-ness
    method assess-eligibility {
        $!eligible = self.age < 21 ?? 'No' !! 'Yes';
    }
}
