#!/usr/bin/env perl6
use v6;

class SomeAttribs {
    has $!attrib1;
    has $!attrib2;
    has $!attrib3;
    has $!attrib4;
    method meth {
        $!attrib1 = 'val 1';
        $!attrib2 = 'val 2';
        $!attrib3 = 'val 3';
        $!attrib4 = 'val 4';
    }
    method show-attribs {
        printf "%s, %s, %s, %s\n",
               $!attrib1,
               $!attrib2,
               $!attrib3,
               $!attrib4
    }
}

my $attribs = SomeAttribs.new;
$attribs.meth;
say $attribs;
$attribs.show-attribs;
